package org.example;

import java.time.LocalDateTime;

public class LoginResponse {
    LocalDateTime time;
    Status status;

    public LoginResponse(LocalDateTime time, Status status) {
        this.time = time;
        this.status = status;
    }

    public static enum Status {
        LOGIN_SUCCESS, LOGIN_FAILED;
    }

    public Test createTest(){
        return new Test();
    }

    public class Test {
        void showTime(){
            System.out.println(time);
        }
    }
}
