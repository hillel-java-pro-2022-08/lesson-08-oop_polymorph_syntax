package org.example;

import org.example.menu.Menu;
import org.example.menu.MenuItem;
import org.example.menu.item.AddMenuItem;
import org.example.menu.item.ExitMenuItem;
import org.example.menu.item.SubstractMenuItem;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Main {

    //Вложенный статический класс
    static class X{
        void showHello(){
            System.out.println("hello");
        }
    }







    public static long profiling(Runnable runnable) {
        long timeStart = System.nanoTime();
        runnable.run();
        long endTime = System.nanoTime();
        return endTime - timeStart;
    }

//
//    assertThrows(Class clazz, Runnable runnable){
//        try {
//            runnable.run();
//            failure()
//        }catch (Exception ex){
//            assertTrue(ex.getClass()==clazz)
//        }
//
//    }


    public static void main(String[] args) {

        LoginResponse loginResponse = new LoginResponse(
                LocalDateTime.now(),
                LoginResponse.Status.LOGIN_FAILED
        );

        LoginResponse.Test test = loginResponse.createTest();
        test.showTime();

        Main.X x = new Main.X();
        x.showHello();

        long time = profiling(() -> {
            for (int i = 0; i < 10000; i++) {
               // System.out.println("s");
            }
        });

        System.out.println(time);





        Scanner scanner = new Scanner(System.in);

        //Anonymous class
        MenuItem multiplication = new MenuItem("Multiplication", false) {
            @Override
            public void run() {
                int first = readNumber("Enter first number");
                int second = readNumber("Enter second number");
                int sum = first * second;
                System.out.println("Mult is " + sum);
            }

            private int readNumber(String string) {
                System.out.print(string + " :");
                int num = scanner.nextInt();
                scanner.nextLine();
                return num;
            }
        };

        Supplier<String> supplier = new Supplier<String>() {
            @Override
            public String get() {
                return "hello";
            }
        };

        Supplier<String> supplier2 = () -> "hello";


        //old school implementation
        Operation sum = new Operation() {
            @Override
            public int exec(int a, int b) {
                return a + b;
            }
        };

        //java8+ (lambda)

        //full lambda
        Operation sum2 = (int a, int b) -> {
            return a + b;
        };

        //if one line
        Operation sum3 = (int a, int b) -> a + b;

        // omit arguments type
        Operation sum4 = (a, b) -> a + b;

        //if one argument (omit brackets)
        Function<Integer, Integer> xx = a -> a + 1;

//        System.out.println(List.of(1, 2, 3, 4, 5).stream()
//                .filter(i -> i % 2 == 0)
//                .map(i -> i * 2)
//                .collect(Collectors.toList())
//        );


        MenuItem[] items = new MenuItem[]{
                new AddMenuItem(scanner),
                new SubstractMenuItem(scanner),
                multiplication,
                new ExitMenuItem()
        };

        for (MenuItem item: items){
            //todo
        }

        for (int i = 0; i < items.length; i++) {
            MenuItem item = items[i];
            //todo
        }

//        Iterator<MenuItem> iter = items.iterator();
//        while(iter.hasNext()){
//            MenuItem item = iter.next();
//            //todo: hghgghklgjfhc
//        }

        Menu menu = new Menu(items, scanner);

        menu.run();
    }
}
