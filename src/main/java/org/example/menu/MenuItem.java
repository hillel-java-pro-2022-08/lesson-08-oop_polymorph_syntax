package org.example.menu;

public abstract class MenuItem {
    private String name;
    private boolean finalItem;

    public MenuItem(String name, boolean finalItem) {
        this.name = name;
        this.finalItem = finalItem;
    }

    public boolean isFinalItem() {
        return finalItem;
    }

    public String getName() {
        return name;
    }

    public abstract void run();
}
