package org.example.menu;

import java.util.Scanner;

public class Menu {

    private final MenuItem[] items;
    private final Scanner scanner;

    public Menu(MenuItem[] items, Scanner scanner) {
        this.items = items;
        this.scanner = scanner;
    }

    public void run() {
        while (true) {
            showMenu();
            int choice = getUserChoice();
            MenuItem active = items[choice];
            active.run();
            if(active.isFinalItem()) break;
        }
    }

    private void showMenu() {
        System.out.println("-----------------------------");
        for (int i = 0; i < items.length; i++) {
            System.out.printf("%d - %s\n", i + 1, items[i].getName());
        }
        System.out.println("-----------------------------");
    }

    private int getUserChoice() {
        System.out.println("Enter your choice");
        int input = scanner.nextInt();
        scanner.nextLine();
        return input - 1;
    }


}
