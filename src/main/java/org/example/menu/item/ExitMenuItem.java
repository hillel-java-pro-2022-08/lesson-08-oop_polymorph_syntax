package org.example.menu.item;

import org.example.menu.MenuItem;

public class ExitMenuItem extends MenuItem {

    public ExitMenuItem() {
        super("Exit", true);
    }

    @Override
    public void run() {
        System.out.println("Good by...");
    }
}
