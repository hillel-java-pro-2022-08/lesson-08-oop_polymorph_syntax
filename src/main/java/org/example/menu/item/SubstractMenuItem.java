package org.example.menu.item;

import org.example.menu.MenuItem;

import java.util.Scanner;

public class SubstractMenuItem extends MenuItem {
    private final Scanner scanner;

    public SubstractMenuItem(Scanner scanner) {
        super("Substruct", false);
        this.scanner = scanner;
    }

    @Override
    public void run() {
        int first = readNumber("Enter first number");
        int second = readNumber("Enter second number");
        int sum = first - second;
        System.out.println("Differance is " + sum);
    }

    private int readNumber(String string) {
        System.out.print(string + " :");
        int num = scanner.nextInt();
        scanner.nextLine();
        return num;
    }
}
