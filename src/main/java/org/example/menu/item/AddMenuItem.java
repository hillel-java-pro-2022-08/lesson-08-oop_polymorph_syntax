package org.example.menu.item;

import org.example.menu.MenuItem;

import java.util.Scanner;

public class AddMenuItem extends MenuItem {
    private final Scanner scanner;

    public AddMenuItem(Scanner scanner) {
        super("Add", false);
        this.scanner = scanner;
    }

    @Override
    public void run() {
        int first = readNumber("Enter first number");
        int second = readNumber("Enter second number");
        int sum = first + second;
        System.out.println("Sum is " + sum);
    }

    private int readNumber(String string) {
        System.out.print(string + " :");
        int num = scanner.nextInt();
        scanner.nextLine();
        return num;
    }
}
