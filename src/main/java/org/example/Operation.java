package org.example;

@FunctionalInterface
public interface Operation {
    int exec(int a, int b);
}
